import React, { useState, useEffect, useCallback } from 'react';
import { StyleSheet, Text, View } from 'react-native';
import { Button, DefaultTheme, Provider } from 'react-native-paper';

export default function App() {
  const [state, setState] = useState([
    { id: '1', value: '' },
    { id: '2', value: '' },
    { id: '3', value: '' },
    { id: '4', value: '' },
    { id: '5', value: '' },
    { id: '6', value: '' },
    { id: '7', value: '' },
    { id: '8', value: '' },
    { id: '9', value: '' }
  ]);

  const [count, setCount] = useState(0);

  const winner = [
    [0, 1, 2],
    [3, 4, 5],
    [6, 7, 8],
    [0, 3, 6],
    [1, 4, 7],
    [2, 5, 8],
    [0, 4, 8],
    [2, 4, 6]
  ];

  const [winnerState, setWinnerState] = useState(false);

  const [myItem, setItem] = useState(null);

  const isWinner = () => {
    const s = count % 2 === 0 ? 'O' : 'X';

    for (let i = 0; i < winner.length; i++) {
      let line = winner[i];
      if (
        state[line[0]].value === s &&
        state[line[1]].value === s &&
        state[line[2]].value === s
      ) {
        setWinnerState(true);
        setCount(0);
      }
    }
    return s;
  };
  useEffect(() => {
    const item = isWinner();
    setItem(item);
  }, [state]);

  const actionHandler = id => {
    const q = state.find(x => x.id === id);

    if (!q.value) {
      if (count % 2 === 0) {
        const item = { id, value: 'X' };
        setState(state.map(x => (x.id === id ? item : x)));
        setCount(count + 1);
      } else {
        const item = { id, value: 'O' };
        setState(state.map(x => (x.id === id ? item : x)));
        setCount(count + 1);
      }
    } else {
      null;
    }
  };

  const content = state.map(x => {
    return (
      <Button
        key={x.id}
        color='#74b9ff'
        mode='contained'
        style={styles.btn}
        onPress={() => actionHandler(x.id)}
        labelStyle={{
          color: '#fff'
        }}
        contentStyle={{
          color: '#fff',
          width: '120%',
          paddingRight: 12,
          height: '100%',
          textAlign: 'center'
        }}
        children={x.value}
      ></Button>
    );
  });

  return winnerState ? (
    <View style={styles.container}>
      <Text style={{ color: '#fff', fontSize: 25 }}>Winner is {myItem}</Text>
      <Button
        mode='contained'
        color='#74b9ff'
        dark={true}
        onPress={() => setWinnerState(false)}
      >
        Reset
      </Button>
    </View>
  ) : (
    <Provider theme={theme} dark={true}>
      <View style={styles.container}>
        <Text style={{ color: '#eee' }}>Game 1</Text>
        <View style={styles.wrap}>{content}</View>
        <Button
          mode='contained'
          dark={true}
          color={theme.colors.accent}
          onPress={() =>
            setState([
              { id: '1', value: '' },
              { id: '2', value: '' },
              { id: '3', value: '' },
              { id: '4', value: '' },
              { id: '5', value: '' },
              { id: '6', value: '' },
              { id: '7', value: '' },
              { id: '8', value: '' },
              { id: '9', value: '' }
            ])
          }
        >
          Reset
        </Button>
      </View>
    </Provider>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    flexDirection: 'column',
    justifyContent: 'center',
    backgroundColor: '#6c5ce7'
  },
  wrap: {
    marginVertical: 10,
    marginHorizontal: 10,
    textAlign: 'center',
    flexDirection: 'row',
    flexWrap: 'wrap',
    justifyContent: 'center',
    height: 240,
    width: 240
  },
  btn: {
    marginVertical: 5,
    marginHorizontal: 5,
    height: 70,
    width: 70,
    justifyContent: 'center',
    alignItems: 'center'
  }
});
