import { all, call } from 'redux-saga/effects';
import sagaList from '../modules/Root/model/saga';

const sagasList = [sagaList];

export default function* watchRootSaga() {
  yield all(sagasList.map(saga => call(saga)));
}
