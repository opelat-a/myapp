import React, { useEffect, useCallback } from 'react';
import uuid from 'uuid/v4';
import { Text } from '../Text/Text';
import { Input } from '../Input/Input';
import { Todo } from '../Todo/Todo';
import { Div, GlobalStyle } from './styledApp';

import { useDispatch, useSelector } from 'react-redux';

import * as action from './model/actions';

export const App = () => {
  const store = useSelector(state => state.list);
  const dispatch = useDispatch();

  const startFetch = useCallback(() => {
    dispatch(action.startFetch());
  }, [dispatch]);

  useEffect(() => {
    startFetch();
  }, []);

  const deleteTodo = useCallback(
    id => {
      dispatch(action.deleteFetch(id));
    },
    [dispatch]
  );
  const addTodo = useCallback(
    text => {
      dispatch(action.postFetch({ id: uuid(), text, completed: false }));
    },
    [dispatch]
  );
  const editTodo = useCallback(
    todo => {
      dispatch(action.editFetch(todo));
    },
    [dispatch]
  );

  const onCheck = useCallback(
    todo => {
      dispatch(action.checkFetch(todo));
    },
    [dispatch]
  );

  const todos = store.map(todo => (
    <Todo key={todo.id} {...{ deleteTodo, onCheck, editTodo, todo }} />
  ));
  return (
    <React.Fragment>
      <Div>
        <Text size={'70px'}>TODO</Text>
        <Input newTodo={addTodo} />
        <Div.todos>{todos}</Div.todos>
      </Div>
      <GlobalStyle />
    </React.Fragment>
  );
};
