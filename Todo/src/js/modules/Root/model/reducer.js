import ActionTypes from '../../../const/ActionTypes';

export default (state = {}, action) => {
  switch (action.type) {
    case ActionTypes.GET_DATA:
      return [...action.payload];
    case ActionTypes.ADD_TODO:
      return [...state, action.payload];
    case ActionTypes.DELETE_TODO:
      return state.filter(todo => action.payload !== todo.id);
    case ActionTypes.EDIT_TODO:
      return state.map(todo =>
        action.payload.id === todo.id ? action.payload : todo
      );
    case ActionTypes.CHECK_TODO:
      return state.map(todo =>
        action.payload.id === todo.id ? action.payload : todo
      );
    default:
      return state;
  }
};
