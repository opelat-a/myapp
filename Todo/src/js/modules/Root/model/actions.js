import ActionTypes from '../../../const/actionTypes';

export const startFetch = () => ({
  type: ActionTypes.GET_DATA_ASYNC
});

export const getState = payload => ({
  type: ActionTypes.GET_DATA,
  payload
});

export const postFetch = payload => ({
  type: ActionTypes.ADD_TODO_ASYNC,
  payload
});

export const addTodo = payload => ({
  type: ActionTypes.ADD_TODO,
  payload
});

export const deleteFetch = payload => ({
  type: ActionTypes.DELETE_TODO_ASYNC,
  payload
});

export const deleteTodo = payload => ({
  type: ActionTypes.DELETE_TODO,
  payload
});

export const editFetch = payload => ({
  type: ActionTypes.EDIT_TODO_ASYNC,
  payload
});

export const editTodo = payload => ({
  type: ActionTypes.EDIT_TODO,
  payload
});

export const checkFetch = payload => ({
  type: ActionTypes.CHECK_TODO_ASYNC,
  payload
});

export const checkTodo = payload => ({
  type: ActionTypes.CHECK_TODO,
  payload
});
