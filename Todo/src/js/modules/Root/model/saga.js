import { takeEvery, put, call } from 'redux-saga/effects';
import ActionTypes from '../../../const/ActionTypes';

import { fetchData, fetchPost, fetchDelete, fetchCheck } from './connection';

import * as actions from './actions';

export default function* watchTestSaga() {
  yield takeEvery(ActionTypes.GET_DATA_ASYNC, getData),
    yield takeEvery(ActionTypes.ADD_TODO_ASYNC, postData),
    yield takeEvery(ActionTypes.EDIT_TODO_ASYNC, editData),
    yield takeEvery(ActionTypes.DELETE_TODO_ASYNC, deleteData),
    yield takeEvery(ActionTypes.CHECK_TODO_ASYNC, checkData);
}

export function* getData() {
  const response = yield call(fetchData);

  yield put(actions.getState(response));
}
export function* postData(action) {
  const response = yield call(fetchPost, action.payload);

  yield put(actions.addTodo(response));
}
export function* deleteData(action) {
  yield call(fetchDelete, action.payload);

  yield put(actions.deleteTodo(action.payload));
}
export function* editData(action) {
  yield call(fetchPost, action.payload);

  yield put(actions.editTodo(action.payload));
}
export function* checkData(action) {
  yield call(fetchCheck, action.payload);

  yield put(actions.checkTodo(action.payload));
}
