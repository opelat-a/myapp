import styled from 'styled-components';

export const ClassInput = styled.input`
  color: #eee;
  border-radius: 10px;
  background-color: transparent;
  padding: 0 10px;
  border: 1px solid #6c5ce7;
  &:focus {
    outline: none;
  }
`;

const inputHeader = styled(ClassInput)`
  padding: 10px 40px;
  font-size: 18px;
`;

ClassInput.header = inputHeader;
