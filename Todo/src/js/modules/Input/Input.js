import React, { useState } from 'react';
import T from 'prop-types';
import { ClassInput } from './styledInput';

export const Input = ({ newTodo, initialValue, style = true }) => {
  const [value, setInputValue] = useState(initialValue);

  const onSubmit = e => {
    e.preventDefault();
    if (value === '') {
      return;
    }
    newTodo(value);
    setInputValue('');
  };

  const onChange = e => {
    setInputValue(e.target.value);
  };

  const currentInput = style ? (
    <ClassInput.header value={value} onChange={onChange} />
  ) : (
    <ClassInput value={value} onChange={onChange} />
  );
  return (
    <form onSubmit={onSubmit} style={{ backgroundColor: 'transparent' }}>
      {currentInput}
    </form>
  );
};

Input.defaultProps = {
  newTodo: () => console.log('Create newTodo'),
  initialValue: '',
  style: true
};

Input.propTypes = {
  newTodo: T.func,
  initialValue: T.string,
  style: T.bool
};
