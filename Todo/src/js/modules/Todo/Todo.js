import React, { useState } from 'react';
import T from 'prop-types';
import { Text } from '../text/Text';
import { Delete } from '../assets/Delete';
import { Checker } from '../assets/Checker';
import { Edit } from '../assets/Edit';
import { Input } from '../input/Input';
// import './style.less';
import { Div } from './styleTodo';

export const Todo = ({ todo, onCheck, deleteTodo, editTodo }) => {
  const [edit, setEdit] = useState(false);

  const editText = text => {
    setEdit(false);
    const newTodo = { ...todo, text };
    editTodo(newTodo);
  };

  const openEdit = () => {
    if (!todo.completed) {
      setEdit(true);
    }
  };

  const onCheckData = id => {
    onCheck({ id, text: todo.text, completed: !todo.completed });
  };

  let text = edit ? (
    <Input style={false} initialValue={todo.text} newTodo={editText} />
  ) : (
    <Text check={todo.completed}>{todo.text}</Text>
  );
  return (
    <Div>
      <Checker todo={todo} onCheck={onCheckData} />
      {text}
      <Edit edit={openEdit} />
      <Delete deleteTodo={deleteTodo} id={todo.id} />
    </Div>
  );
};

Todo.defaultProps = {
  todo: {
    id: '1',
    text: 'Hello',
    completed: false
  },
  onCheck: () => {
    console.log('check');
  },
  deleteTodo: () => {
    console.log('delete');
  },
  editTodo: () => {
    console.log('edit');
  }
};

Todo.propTypes = {
  todo: T.shape({
    id: T.string.isRequired,
    text: T.string.isRequired,
    completed: T.bool.isRequired
  }),
  onCheck: T.func.isRequired,
  deleteTodo: T.func.isRequired,
  editTodo: T.func.isRequired
};
