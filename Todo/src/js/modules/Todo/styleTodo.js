import styled from 'styled-components';

export const Div = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: space-around;
  align-items: center;
  padding: 10px 0;
  width: 300px;
  border-radius: 10px;
  margin: 10px 0;
  background: linear-gradient(45deg, #a29bfe, #6c5ce7);
`;
