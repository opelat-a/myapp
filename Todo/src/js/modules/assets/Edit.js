import React from 'react';
import T from 'prop-types';

import { faPenSquare } from '@fortawesome/free-solid-svg-icons';
import { Div, AwesomeIcon } from './styledAssets';

export const Edit = ({ edit }) => {
  return (
    <Div onClick={edit}>
      <AwesomeIcon icon={faPenSquare} style={{ fontSize: '24px' }} />
    </Div>
  );
};

Edit.defaultProps = {
  edit: () => {}
};

Edit.propTypes = {
  edit: T.func.isRequired
};
