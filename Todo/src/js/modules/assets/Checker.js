import React, { useState } from 'react';
import T from 'prop-types';
// import './style.less';

export const Checker = ({ todo, onCheck }) => {
  const [checked, setChecked] = useState(todo.completed);

  const onChange = e => {
    onCheck(e.target.value);
    setChecked(!checked);
  };

  return (
    <input
      type='checkbox'
      value={todo.id}
      onChange={onChange}
      checked={checked}
    />
  );
};

Checker.defaultProps = {
  todo: {
    id: '1',
    text: 'Hello',
    completed: false
  },
  onCheck: () => console.log('where my onCheck')
};

Checker.propTypes = {
  todo: T.shape({
    id: T.string.isRequired,
    text: T.string.isRequired,
    completed: T.bool.isRequired
  }),
  onCheck: T.func.isRequired
};
