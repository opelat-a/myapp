export default Object.defineProperties(
  {},
  {
    GET_DATA: { value: 'get_data', writable: false },
    GET_DATA_ASYNC: { value: 'get_data_async', writable: false },
    ADD_TODO_ASYNC: { value: 'add_todo_async', writable: false },
    ADD_TODO: { value: 'add_todo', writable: false },
    DELETE_TODO_ASYNC: { value: 'delete_todo_async', writable: false },
    DELETE_TODO: { value: 'delete_todo', writable: false },
    EDIT_TODO_ASYNC: { value: 'edit_todo_async', writable: false },
    EDIT_TODO: { value: 'edit_todo', writable: false },
    CHECK_TODO_ASYNC: { value: 'check_todo_async', writable: false },
    CHECK_TODO: { value: 'check_todo', writable: false }
  }
);
