import React from 'react';
import T from 'prop-types';
import { AwesomeIcon, Div } from './styledAssets';

import { faCheckSquare, faSquare } from '@fortawesome/free-regular-svg-icons';

export const Checker = ({ todo, onCheck }) => {
  const onChange = e => {
    if (e.target.dataset.value) {
      onCheck(e.target.dataset.value);
    } else {
      return null;
    }
  };
  const icon = todo.completed ? faCheckSquare : faSquare;

  return (
    <Div onClick={onChange}>
      <AwesomeIcon icon={icon} data-value={todo.id} />
    </Div>
  );
};

Checker.defaultProps = {
  todo: {
    id: '1',
    text: 'Hello',
    completed: false
  },
  onCheck: () => console.log('where my onCheck')
};

Checker.propTypes = {
  todo: T.shape({
    id: T.string.isRequired,
    text: T.string.isRequired,
    completed: T.bool.isRequired
  }),
  onCheck: T.func.isRequired
};
