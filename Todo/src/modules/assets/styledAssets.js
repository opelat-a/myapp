import styled from 'styled-components';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

export const Div = styled.div`
  display: flex;
  background-color: transparent;
  justify-content: center;
`;

export const AwesomeIcon = styled(FontAwesomeIcon)`
  background-color: transparent;
  color: #fdcb6e;
  font-size: 22px;
  align-self: center;
  &:hover {
    color: #ffeaa7;
  }
`;
