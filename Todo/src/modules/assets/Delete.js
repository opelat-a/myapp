import React from 'react';
import T from 'prop-types';

import { faTrash } from '@fortawesome/free-solid-svg-icons';

import { Div, AwesomeIcon } from './styledAssets';

export const Delete = ({ deleteTodo, id }) => {
  return (
    <Div onClick={() => deleteTodo(id)}>
      <AwesomeIcon icon={faTrash} />
    </Div>
  );
};

Delete.defaultProps = {
  deleteTodo: () => {},
  id: '1'
};

Delete.propTypes = {
  deleteTodo: T.func.isRequired,
  id: T.string.isRequired
};
