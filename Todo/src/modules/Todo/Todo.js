import React, { useState } from 'react';
import T from 'prop-types';
import { Text } from '../Text/Text';
import { Delete } from '../assets/Delete';
import { Checker } from '../assets/Checker';
import { Edit } from '../assets/Edit';
import { Input } from '../Input/Input';
import { Div } from './styleTodo';

import Spinner from '../Spinner/Spinner';

export const Todo = ({ todo, onCheck, deleteTodo, editTodo }) => {
  const [edit, setEdit] = useState(false);

  const editText = text => {
    setEdit(false);
    const newTodo = { ...todo, text };
    editTodo(newTodo);
  };

  const openEdit = () => {
    if (!todo.completed) {
      setEdit(true);
    }
  };

  const onCheckData = id => {
    onCheck({ id, text: todo.text, completed: !todo.completed });
  };

  let text = edit ? (
    <Input currentStyle={false} initialValue={todo.text} newTodo={editText} />
  ) : (
    <Text check={todo.completed}>{todo.text}</Text>
  );
  const getBody = todo.isFetching ? (
    <Spinner />
  ) : (
    <>
      <Checker todo={todo} onCheck={onCheckData} />
      {text}
      <Edit edit={openEdit} />
      <Delete deleteTodo={deleteTodo} id={todo.id} />
    </>
  );
  return <Div>{getBody}</Div>;
};

Todo.defaultProps = {
  todo: {
    id: '1',
    text: 'Hello',
    completed: false
  },
  onCheck: () => {
    console.log('check');
  },
  deleteTodo: () => {
    console.log('delete');
  },
  editTodo: () => {
    console.log('edit');
  }
};

Todo.propTypes = {
  todo: T.shape({
    id: T.string.isRequired,
    text: T.string,
    completed: T.bool,
    isFetching: T.bool
  }),
  onCheck: T.func.isRequired,
  deleteTodo: T.func.isRequired,
  editTodo: T.func.isRequired
};
