import React, { useState } from 'react';
import T from 'prop-types';
import { ClassInput } from './styledInput';

export const Input = ({ newTodo, initialValue, currentStyle = true }) => {
  const [value, setInputValue] = useState(initialValue);

  const onSubmit = event => {
    event.preventDefault();
    if (value === '') {
      return;
    }
    newTodo(value);
    setInputValue('');
  };

  const onChange = e => {
    setInputValue(e.target.value);
  };

  const currentInput = currentStyle ? (
    <ClassInput.header value={value} onChange={onChange} />
  ) : (
    <ClassInput value={value} onChange={onChange} />
  );
  return (
    <form onSubmit={onSubmit} style={{ backgroundColor: 'transparent' }}>
      {currentInput}
    </form>
  );
};

Input.defaultProps = {
  newTodo: () => console.log('Create newTodo'),
  initialValue: '',
  currentStyle: true
};

Input.propTypes = {
  newTodo: T.func,
  initialValue: T.string,
  currentStyle: T.bool
};
