import styled, { createGlobalStyle } from 'styled-components';

export const GlobalStyle = createGlobalStyle`
body {
    background-color: #2d3436;
     font-family: 'Poppins', sans-serif;
}
`;

export const Div = styled.div`
  padding: 20px;
  display: flex;
  justify-content: center;
  flex-direction: column;
  align-items: center;
`;

const todos = styled.div`
  display: flex;
  flex-direction: column;
`;

Div.todos = todos;
