import ActionTypes from '../../../const/actionTypes';

export default (state = {}, action) => {
  switch (action.type) {
    case ActionTypes.GET_DATA:
      return [...action.payload];
    case ActionTypes.ADD_TODO:
      return [...state, action.payload];
    case ActionTypes.DELETE_TODO:
      return state.filter(todo => action.payload !== todo.id);
    case ActionTypes.EDIT_TODO:
      return state.map(todo =>
        action.payload.id === todo.id ? action.payload : todo
      );
    case ActionTypes.CHECK_TODO:
      return state.map(todo =>
        action.payload.id === todo.id ? action.payload : todo
      );
    case ActionTypes.START_FETCHING:
      return [
        ...state,
        { id: new Date().toString(), text: null, isFetching: true }
      ];
    case ActionTypes.STOP_FETCHING:
      return state.slice(0, -1);
    case ActionTypes.START_FETCHING_TARGET:
      return state.map(todo =>
        todo.id === action.payload ? { ...todo, isFetching: true } : todo
      );
    default:
      return state;
  }
};
