export const fetchData = async () => {
  const url = 'https://test-app-630a0.firebaseio.com/Todo.json';

  const response = await fetch(url, {
    method: 'GET',
    headers: { 'Content-Type': 'application/json' }
  });
  const data = await response.json();

  const dataFb = Object.keys(data).map(key => ({ ...data[key] }));
  return dataFb;
};

export const fetchPost = async ({ id, text, completed, isFetching }) => {
  const url = `https://test-app-630a0.firebaseio.com/Todo/${id}.json`;

  const response = await fetch(url, {
    method: 'PUT',
    headers: { 'Content-Type': 'application/json' },
    body: JSON.stringify({ id, text, completed })
  });

  const data = await response.json();

  return data;
};

export const fetchDelete = async id => {
  const url = `https://test-app-630a0.firebaseio.com/Todo/${id}.json`;

  await fetch(url, {
    method: 'DELETE',
    headers: { 'Content-Type': 'application/json' }
  });
};

export const fetchCheck = async ({ id, text, completed }) => {
  const url = `https://test-app-630a0.firebaseio.com/Todo/${id}.json`;

  const response = await fetch(url, {
    method: 'PUT',
    headers: { 'Content-Type': 'application/json' },
    body: JSON.stringify({ id, text, completed })
  });

  const data = await response.json();

  return data;
};
