import { put, call, select } from 'redux-saga/effects';
import { cloneableGenerator } from '@redux-saga/testing-utils';
import { expectSaga } from 'redux-saga-test-plan';

import { getData, postData, deleteData, editData } from '../saga';

import { fetchData, fetchPost, fetchDelete, fetchCheck } from '../connection';

import * as actions from '../actions';

const actionPayload = {
  id: '1',
  text: 'new',
  completed: false
};

const response = [{ id: '1', text: '1', completed: false }];

const getDataAction = actions.startFetch();
const postDataAction = actions.postFetch(actionPayload);

const sagaGetData = cloneableGenerator(getData)(getDataAction);
const sagaPostData = cloneableGenerator(postData)(postDataAction);

describe('getPost', () => {
  test('should call to get data', () => {
    expect(sagaGetData.next().value).toEqual(call(fetchData));
  });
  test('should dispatch response to store', () => {
    expect(sagaGetData.next(response).value).toEqual(
      put(actions.getState(response))
    );
  });
});

describe('saga- postData', () => {
  test('should dispatch startFetching action', () => {
    expect(sagaPostData.next().value).toEqual(put(actions.startFetching()));
  });

  test('should call fetch request', () => {
    expect(sagaPostData.next().value).toEqual(call(fetchPost, actionPayload));
  });

  test('should dispatch stopFetching action', () => {
    expect(sagaPostData.next(actionPayload).value).toEqual(
      put(actions.stopFetching())
    );
  });

  test('should put addTodo dispatch', () => {
    expect(sagaPostData.next(actionPayload).value).toMatchInlineSnapshot(`
      Object {
        "@@redux-saga/IO": true,
        "combinator": false,
        "payload": Object {
          "action": Object {
            "payload": Object {
              "completed": false,
              "id": "1",
              "text": "new",
            },
            "type": "add_todo",
          },
          "channel": undefined,
        },
        "type": "PUT",
      }
    `);
    // toEqual(
    //     put(actions.addTodo(actionPayload)))
  });
});

describe('deleteData', () => {
  const action = {
    payload: '7'
  };
  test('deletePost', async () => {
    await expectSaga(deleteData, action)
      .put(actions.startFetchingTarget(action.payload))
      .call(fetchDelete, action.payload)
      .put(actions.deleteTodo(action.payload))
      .run(false);
  });
});

describe('editPost', () => {
  const action = {
    payload: { id: '2', text: '222', completed: false }
  };

  test('edit', async () => {
    await expectSaga(editData, action)
      .put(actions.startFetchingTarget(action.payload.id))
      .call(fetchPost, action.payload)
      .put(actions.editTodo(action.payload))
      .run(false);
  });
});
