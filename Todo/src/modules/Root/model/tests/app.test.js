import React from 'react';
import { Provider } from 'react-redux';
import renderer from 'react-test-renderer';
import configureStore from 'redux-mock-store';
import { App } from '../../App';
import * as actions from '../actions';

const mockStore = configureStore([]);

describe('My Connected React-Redux Component', () => {
  let store;
  let component;

  beforeEach(() => {
    store = mockStore({
      list: [
        { id: '1', text: 'hello', completed: false },
        { id: '2', text: 'tests', completed: false }
      ]
    });
    component = renderer.create(
      <Provider store={store}>
        <App />
      </Provider>
    );
  });
  it('should render with given state from Redux store', () => {
    expect(component.toJSON()).toMatchSnapshot();
  });
  //   it('should dispatch an action on button click', () => {
  //     renderer.act(() => {
  //       component.root.findByType('form').props.onSubmit();
  //     });
  //     expect(store.dispatch).toHaveBeenCalledTimes(1);
  //     expect(store.dispatch).toHaveBeenCalledWith(
  //       actions.addTodo({ id: '3', text: 'dispatch', completed: false })
  //     );
  //   });
});
