import reducer from '../reducer';

import * as actions from '../actions';

describe('Reducer', () => {
  it('default state', () => {
    expect(reducer(void 0, {})).toEqual({});
  });

  it('Get_Data', () => {
    const state = [];
    const payload = [4, 5];

    const action = actions.getState(payload);

    expect(reducer(state, action)).toEqual(payload);
  });

  it('Add Todo', () => {
    const state = [{ name: '1' }];
    const payload = { id: '2' };

    const action = actions.addTodo(payload);

    expect(reducer(state, action)).toEqual([...state, payload]);
  });

  it('Delete Todo', () => {
    const state = [{ id: '1' }, { id: '2' }];
    const payload = '2';

    const expected = [{ id: '1' }];

    const action = actions.deleteTodo(payload);

    expect(reducer(state, action)).toEqual(expected);
  });

  it('Edit Todo', () => {
    const state = [
      { id: '1', q: 'hello' },
      { id: '2', name: 'hello1' }
    ];
    const payload = { id: '2', name: 'hello2' };

    const expected = [
      { id: '1', q: 'hello' },
      { id: '2', name: 'hello2' }
    ];

    const action = actions.editTodo(payload);

    expect(reducer(state, action)).toEqual(expected);
  });

  it('Check Todo', () => {
    const state = [
      { id: '1', completed: false },
      { id: '2', completed: false }
    ];
    const payload = { id: '2', completed: true };

    const expected = [
      { id: '1', completed: false },
      { id: '2', completed: true }
    ];

    const action = actions.checkTodo(payload);

    expect(reducer(state, action)).toEqual(expected);
  });

  it('Start Fetch', () => {
    const state = [
      { id: '1', isFetching: false },
      { id: '2', isFetching: false }
    ];

    const expected = [
      { id: '1', isFetching: false },
      { id: '2', isFetching: false },
      { id: new Date().toString(), isFetching: true, text: null }
    ];

    const action = actions.startFetching();

    expect(reducer(state, action)).toEqual(expected);
  });

  it('Stop Fetch', () => {
    const state = [
      { id: '1', isFetching: false },
      { id: '2', isFetching: false }
    ];

    const expected = [{ id: '1', isFetching: false }];

    const action = actions.stopFetching();

    expect(reducer(state, action)).toEqual(expected);
  });

  it('Start Fetch Target', () => {
    const state = [
      { id: '1', isFetching: false },
      { id: '2', isFetching: false }
    ];
    const payload = '2';

    const expected = [
      { id: '1', isFetching: false },
      { id: '2', isFetching: true }
    ];

    const action = actions.startFetchingTarget(payload);

    expect(reducer(state, action)).toEqual(expected);
  });
});
