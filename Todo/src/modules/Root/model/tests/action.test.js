import * as actions from '../actions';
import TYPE from '../../../../const/actionTypes';

describe('Test Actions', () => {
  it('StartFetch', () => {
    const expectedAction = {
      type: TYPE.GET_DATA_ASYNC
    };
    expect(actions.startFetch()).toEqual(expectedAction);
  });

  it('Add Todo', () => {
    const payload = 'Some Text';
    const expectedAction = {
      type: TYPE.ADD_TODO,
      payload
    };
    expect(actions.addTodo(payload)).toEqual(expectedAction);
  });

  it('Get State', () => {
    const payload = 'Some Text';
    const expectedAction = {
      type: TYPE.GET_DATA,
      payload
    };
    expect(actions.getState(payload)).toEqual(expectedAction);
  });

  it('deleteFetch', () => {
    const payload = '2';
    const expectedAction = {
      type: TYPE.DELETE_TODO_ASYNC,
      payload
    };
    expect(actions.deleteFetch(payload)).toEqual(expectedAction);
  });

  it('deleteTodo', () => {
    const payload = '2';
    const expectedAction = {
      type: TYPE.DELETE_TODO,
      payload
    };
    expect(actions.deleteTodo(payload)).toEqual(expectedAction);
  });
});
