import React from 'react';
import T from 'prop-types';

import { Div } from './styledText';

export const Text = ({ children, size, check }) => {
  let color =
    check === true ? { color: '#ff7675', textDecoration: 'line-through' } : {};

  return <Div style={{ fontSize: size, ...color }}>{children}</Div>;
};

Text.defaultProps = {
  children: 'hello',
  size: '20px',
  check: false
};

Text.propTypes = {
  children: T.string,
  size: T.string,
  check: T.bool
};
