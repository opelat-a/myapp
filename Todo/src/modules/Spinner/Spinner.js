import React from 'react';
import { css } from '@emotion/core';

import { CircleLoader } from 'react-spinners';

const override = css`
  display: block;
  margin: 0 auto;
  border-color: red;
`;

const AwesomeComponent = () => {
  return (
    <div className='sweet-loading'>
      <CircleLoader css={override} size={30} color={'#fdcb6e'} loading={true} />
    </div>
  );
};

export default AwesomeComponent;
