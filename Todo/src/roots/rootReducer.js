import { combineReducers } from 'redux';
import reducer from '../modules/Root/model/reducer';

const rootReducer = combineReducers({
  list: reducer
});

export default rootReducer;
