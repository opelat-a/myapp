import React from 'react';
import { render } from 'react-dom';

import { App } from './modules/Root/App';

import { compose, createStore, applyMiddleware } from 'redux';
import { Provider } from 'react-redux';
import createSagaMiddleware from 'redux-saga';

import rootReducer from './roots/rootReducer';
import rootSaga from './roots/rootSaga';

document.addEventListener('DOMContentLoaded', () => {
  init();
});

function init() {
  const initialState = {
    list: []
  };

  const sagaMiddleware = createSagaMiddleware({
    onError: error => {
      alert('Critical error acquired! See console for more details.');
      console.error(error);
      sagaMiddleware.run(rootSaga);
    }
  });

  const composeEnhancers =
    window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

  const store = createStore(
    rootReducer,
    initialState,
    composeEnhancers(applyMiddleware(sagaMiddleware)) // TODO uncomment to work with SAGA
  );

  sagaMiddleware.run(rootSaga);

  render(
    <Provider store={store}>
      <App />
    </Provider>,
    document.getElementById('root')
  );
}

export function setFavIcon(src) {
  if (!src) {
    return false;
  }

  const favicon = document.createElement('link');

  favicon.rel = 'icon';
  favicon.href = src;
  document.head.appendChild(favicon);
}
